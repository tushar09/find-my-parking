package corp.tz.findmyparking;

import org.greenrobot.greendao.generator.DaoGenerator;
import org.greenrobot.greendao.generator.Entity;
import org.greenrobot.greendao.generator.Schema;

public class Generator{
    private static final String PROJECT_DIR = System.getProperty("user.dir");
    private static final int DB_VERSION = 17;
    private static final String DEFAULT_PACKAGE = "corp.tz.findmyparking.db";

    public static void main(String[] args) {
        Schema schema = new Schema(DB_VERSION, DEFAULT_PACKAGE);
        schema.enableKeepSectionsByDefault();

        addTables(schema);

        try {
            new DaoGenerator().generateAll(schema, PROJECT_DIR  + "//app//src//main//java");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void addTables(final Schema schema) {
        addProfile(schema);
        addMySpots(schema);
        addMySpotsWithFrequency(schema);
    }

    private static Entity addProfile(Schema schema){
        Entity profile = schema.addEntity("Profile");
        profile.addLongProperty("id").primaryKey().autoincrement();
        profile.addStringProperty("email").unique();
        profile.addStringProperty("image");
        profile.addStringProperty("password");
        profile.addStringProperty("phone").unique();
        profile.addStringProperty("name");
        profile.addStringProperty("reg").unique();
        profile.addStringProperty("lic").unique();
        profile.addStringProperty("uid").unique();
        return profile;
    }

    private static Entity addMySpots(Schema schema){
        Entity mySpots = schema.addEntity("MySpots");
        mySpots.addLongProperty("id").primaryKey().autoincrement();
        mySpots.addStringProperty("name");
        mySpots.addStringProperty("date").unique();
        mySpots.addLongProperty("timeInLong");
        mySpots.addStringProperty("origin");
        mySpots.addStringProperty("destination");
        mySpots.addStringProperty("uid");
        mySpots.addIntProperty("frequency");
        mySpots.addIntProperty("requiredTime");
        mySpots.addIntProperty("placeId");
        return mySpots;
    }

    private static Entity addMySpotsWithFrequency(Schema schema){
        Entity mySpots = schema.addEntity("MySpotsFrequency");
        mySpots.addLongProperty("id").primaryKey().autoincrement();
        mySpots.addStringProperty("name");
        mySpots.addStringProperty("date").unique();
        mySpots.addLongProperty("timeInLong");
        mySpots.addStringProperty("origin");
        mySpots.addStringProperty("destination").unique();
        mySpots.addIntProperty("frequency");
        return mySpots;
    }
}
