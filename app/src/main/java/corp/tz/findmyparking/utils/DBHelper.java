package corp.tz.findmyparking.utils;

import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;

import org.greenrobot.greendao.query.WhereCondition;

import java.util.List;

import corp.tz.findmyparking.db.DaoMaster;
import corp.tz.findmyparking.db.DaoSession;
import corp.tz.findmyparking.db.MySpots;
import corp.tz.findmyparking.db.MySpotsDao;
import corp.tz.findmyparking.db.MySpotsFrequency;
import corp.tz.findmyparking.db.MySpotsFrequencyDao;
import corp.tz.findmyparking.db.Profile;
import corp.tz.findmyparking.db.ProfileDao;

/**
 * Created by Tushar on 7/26/2017.
 */

public class DBHelper{

    private SQLiteOpenHelper sqLiteOpenHelper;
    private DaoMaster daoMaster;
    private DaoSession daoSession;
    private ProfileDao profileDao;
    private MySpotsDao mySpotsDao;
    private MySpotsFrequencyDao mySpotsFrequencyDao;

    private final String DB_NAME = "find-my-parking-db";

    public DBHelper(Context context){
        sqLiteOpenHelper = new DaoMaster.DevOpenHelper(context, DB_NAME, null);
        daoMaster = new DaoMaster(sqLiteOpenHelper.getWritableDatabase());
        daoSession = daoMaster.newSession();
        profileDao = daoSession.getProfileDao();
        mySpotsDao = daoSession.getMySpotsDao();
        mySpotsFrequencyDao = daoSession.getMySpotsFrequencyDao();
    }

    public Profile getProfileByEmail(String email){
        return profileDao.queryBuilder().where(ProfileDao.Properties.Email.eq(email)).unique();
    }

    public void updateProfile(Profile profile){
        profileDao.insertOrReplace(profile);
    }

    public void inserMySpots(MySpots mySpots){
        long count = mySpotsDao.queryBuilder().where(MySpotsDao.Properties.Destination.eq(mySpots.getDestination())).count();
//        if(spot == null){
//            mySpots.setFrequency(0);
//            mySpotsDao.insertOrReplace(mySpots);
//        }else {
//            mySpots.setFrequency(spot.getFrequency() + 1);
//            mySpotsDao.insertOrReplace(mySpots);
//        }

        mySpots.setFrequency((int)count + 1);
        mySpotsDao.insertOrReplace(mySpots);

    }

    public void insertMySpotsByFrequency(MySpotsFrequency mySpots){
        MySpotsFrequency spot = mySpotsFrequencyDao.queryBuilder().where(MySpotsFrequencyDao.Properties.Destination.eq(mySpots.getDestination())).unique();
        if(spot == null){
            mySpots.setFrequency(0);
            mySpotsFrequencyDao.insertOrReplace(mySpots);
        }else{
            mySpots.setFrequency(spot.getFrequency() + 1);
            mySpotsFrequencyDao.insertOrReplace(mySpots);
        }
    }

    public List<MySpots> getMySpotsAsList(){
        return mySpotsDao.queryBuilder().list();
    }

    public List<MySpotsFrequency> getMySpotsAsListByFrequency(){
        return mySpotsFrequencyDao.queryBuilder().orderDesc(MySpotsFrequencyDao.Properties.Frequency).list();
    }
}
