package corp.tz.findmyparking.utils;

import android.location.Location;

import corp.tz.findmyparking.dto.MapDirectionResponseDto;

/**
 * Created by Tushar on 7/30/2017.
 */

public class RouteTracker{

    private MapDirectionResponseDto.routes routes;
    private int tracker;
    private String latlng;

    public RouteTracker(MapDirectionResponseDto.routes routes, int tracker, String latlng){
        this.routes = routes;
        this.tracker = tracker;
        this.latlng = latlng;
    }

    public MapDirectionResponseDto.routes getRoutes(){
        return routes;
    }

    public void setRoutes(MapDirectionResponseDto.routes routes){
        this.routes = routes;
    }

    public int getTracker(){
        return tracker;
    }

    public void setTracker(int tracker){
        this.tracker = tracker;
    }

    public String getLatlng(){
        return latlng;
    }

    public void setLatlng(String latlng){
        this.latlng = latlng;
    }
}
