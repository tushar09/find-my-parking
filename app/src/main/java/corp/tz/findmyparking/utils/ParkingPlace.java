package corp.tz.findmyparking.utils;

/**
 * Created by Tushar on 8/18/2017.
 */

public class ParkingPlace{

    double lat, lng;
    int capacity, freeNow, id;

    public double getLat(){
        return lat;
    }

    public void setLat(double lat){
        this.lat = lat;
    }

    public double getLng(){
        return lng;
    }

    public void setLng(double lng){
        this.lng = lng;
    }

    public int getCapacity(){
        return capacity;
    }

    public void setCapacity(int capacity){
        this.capacity = capacity;
    }

    public int getFreeNow(){
        return freeNow;
    }

    public void setFreeNow(int freeNow){
        this.freeNow = freeNow;
    }

    public int getId(){
        return id;
    }

    public void setId(int id){
        this.id = id;
    }
}
