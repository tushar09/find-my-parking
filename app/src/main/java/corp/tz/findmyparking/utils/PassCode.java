package corp.tz.findmyparking.utils;

/**
 * Created by Tushar on 8/18/2017.
 */

public class PassCode{
    String uid, passCode;
    int placeId;

    public String getUid(){
        return uid;
    }

    public void setUid(String uid){
        this.uid = uid;
    }

    public String getPassCode(){
        return passCode;
    }

    public void setPassCode(String passCode){
        this.passCode = passCode;
    }

    public int getPlaceId(){
        return placeId;
    }

    public void setPlaceId(int placeId){
        this.placeId = placeId;
    }
}
