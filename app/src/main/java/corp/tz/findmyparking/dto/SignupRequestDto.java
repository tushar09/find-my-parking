package corp.tz.findmyparking.dto;

/**
 * Created by Tushar on 7/22/2017.
 */

public class SignupRequestDto{
    String phoneNumber;

    public String getPhoneNumber(){
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber){
        this.phoneNumber = phoneNumber;
    }
}
