package corp.tz.findmyparking.dto;

/**
 * Created by Tushar on 7/22/2017.
 */

public class SignupResponseDto{
    String success;

    public String getSuccess(){
        return success;
    }

    public void setSuccess(String success){
        this.success = success;
    }
}
