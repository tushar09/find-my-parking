package corp.tz.findmyparking.service;

import corp.tz.findmyparking.dto.MapDirectionResponseDto;
import corp.tz.findmyparking.dto.SignupRequestDto;
import corp.tz.findmyparking.dto.SignupResponseDto;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Url;


public interface ApiService{
    @GET
    Call<MapDirectionResponseDto> getDirection(@Url String url);

    @POST("signup.php")
    Call<SignupResponseDto> signUp(@Body SignupRequestDto dto);
}
